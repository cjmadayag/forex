import React from "react";
import RateRow from "./RateRow";

const ExchangeRates = ({baseCurrencyRates,baseCurrency})=>{
  return(
    <div
      className="d-flex flex-column"
    >
      {baseCurrencyRates.map((baseCurrencyRate,index)=>{
        return(
          <RateRow
            key={index}
            rate={baseCurrencyRate}
            baseCurrency={baseCurrency}
          />
        )
      })}
    </div>
  )
}

export default ExchangeRates;