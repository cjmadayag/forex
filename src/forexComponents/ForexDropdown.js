import React from "react";
import { FormGroup,Dropdown,DropdownToggle,DropdownMenu,Label, DropdownItem } from "reactstrap";
import Currencies from "./ForexData";

const ForexDropdown = ({label,setCurrency,currency})=>{

  const [dropdownIsOpen,setDropdownIsOpen] = React.useState(false);

  return(
    <FormGroup>
      <Label>{label}</Label>
      <Dropdown
        isOpen = {dropdownIsOpen}
        toggle = {()=>setDropdownIsOpen(!dropdownIsOpen)}
      >
        <DropdownToggle caret>{currency == null ? "Choose Currency" : currency.currency}</DropdownToggle>
        <DropdownMenu>
          {Currencies.map((currency,index)=>(
            <DropdownItem
              key={index}
              onClick={()=>setCurrency(currency)}
            >{currency.currency}</DropdownItem>
          ))}
        </DropdownMenu>
      </Dropdown>
    </FormGroup>
  )
}

export default ForexDropdown;