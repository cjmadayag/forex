import React from "react";
import {FormGroup,Label,Input} from "reactstrap";

const ForexInput = ({label,placeholder,setAmount})=>{
  return(
    <FormGroup>
      <Label>{label}</Label>
      <Input
        placeholder={placeholder}
        onChange={e=>setAmount(e.target.value)}
        type = "number"
      />
    </FormGroup>
  )
}

export default ForexInput;