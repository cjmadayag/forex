import React from "react";

const RateRow = ({rate,baseCurrency})=>{
  return (
    <>
      {(baseCurrency.code==rate[0])
      ?
      <span className="font-weight-bold">{rate[0]}:{rate[1]}</span> 
      :
        <span>{rate[0]}:{rate[1]}</span>
      }
    </> 
  )
}

export default RateRow;