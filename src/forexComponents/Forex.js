import React from "react";
import ForexDropdown from "./ForexDropdown";
import ForexInput from "./ForexInput";
import {Button} from "reactstrap";
import Currencies from "./ForexData";
import ExchangeRates from "./ExchangeRates";

const Forex = ()=>{

  const [amount,setAmount] = React.useState();
  const [baseCurrency,setBaseCurrency] = React.useState();
  const [targetCurrency,setTargetCurrency] = React.useState();
  const [convertedAmount,setConvertedAmount] = React.useState();
  const [baseCurrencyRates, setBaseCurrencyRates] = React.useState([]);

  const handleConvert = ()=>{
    if(baseCurrency!=null && targetCurrency!=null && amount!=null && amount>0 ){
      fetch("https://api.exchangeratesapi.io/latest?base="+baseCurrency.code)
      .then(res=>res.json())
      .then(res=>{
        setConvertedAmount([amount*res.rates[targetCurrency.code],targetCurrency.code]);
      })
    }
  }

  const handleBaseCurrency = currency =>{
    setBaseCurrency(currency);
    
    fetch("https://api.exchangeratesapi.io/latest?base="+currency.code)
    .then(res=>res.json())
    .then(res=>{
      const rates = Currencies.map(currency=>{
        return (
          [currency.code,res.rates[currency.code]!=null?res.rates[currency.code].toFixed(2):"1.00"]
        )
      })
      setBaseCurrencyRates(rates)
    })
  }

  return (
    <div
      className="border rounded"
      style={{width:"50%"}}
    >
      <h1 className="text-center my-5">Forex Calculator</h1>
      <div
        className="d-flex justify-content-around"
        // style={{margin:"0 200px"}}
      >
        <ForexDropdown
          label="Base Currency"
          setCurrency = {handleBaseCurrency}
          currency = {baseCurrency}
        />
        <ExchangeRates
          baseCurrencyRates={baseCurrencyRates}
          baseCurrency={baseCurrency}
        />
        <ForexDropdown
          label="Target Currency"
          setCurrency = {(currency)=>setTargetCurrency(currency)}
          currency = {targetCurrency}
        />
      </div>
      <div
        className="d-flex justify-content-around"
      >
        <ForexInput
          label="Amount"
          placeholder="Amount to convert"
          setAmount = {setAmount}
          amount = {amount}
        />
        <Button
          color="info"
          onClick = {handleConvert}
        >
        Convert
        </Button>
      </div>
      <div>
        <h1 className="text-center">{convertedAmount!=null && convertedAmount[0] + " " + convertedAmount[1]}</h1>
      </div>
    </div>
  )
}

export default Forex;